package it.sanmauro.budoapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import it.sanmauro.budoapp.database.DatabaseManager;
import it.sanmauro.budoapp.model.Document;
import it.sanmauro.budoapp.model.Event;
import it.sanmauro.budoapp.model.Message;

public class MainActivity extends AppCompatActivity implements MessageFragment.OnMessageClickedListener,
        DocumentFragment.OnDocumentClickedListener, EventFragment.OnEventClickedListener {

    private DatabaseManager dbManager = new DatabaseManager(MainActivity.this);

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_messages:
                    Utilities.getInstance().setCurrentFragment(Utilities.F.MESSAGES);
                    replaceFragment(MessageFragment.newInstance(1), false, false);
                    return true;
                case R.id.navigation_documents:
                    Utilities.getInstance().setCurrentFragment(Utilities.F.DOCUMENTS);
                    replaceFragment(DocumentFragment.newInstance(2), false, false);
                    return true;
                case R.id.navigation_events:
                    Utilities.getInstance().setCurrentFragment(Utilities.F.EVENTS);
                    replaceFragment(EventFragment.newInstance(1), false, false);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Utilities.getInstance().getStringFromSP(MainActivity.this, Utilities.KEY_EMAIL).isEmpty()) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation_bar);
        navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        replaceFragment(Utilities.getInstance().getCurrentFragment(), false, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_account:
                startActivity(new Intent(MainActivity.this, AccountActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    private void replaceFragment(Fragment fragment, boolean animation, boolean back) {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
        }
        FragmentTransaction transaction = manager.beginTransaction();
        if (animation) {
            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        }
        transaction.replace(R.id.container, fragment);
        if (back) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private boolean fileExists(String fileName) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File file = new File(Utilities.getInstance().getDirDocs(), fileName);
            return file.exists();
        }
        return false;
    }

    private void openFile(String fileName) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File file = new File(Utilities.getInstance().getDirDocs(), fileName);
            Uri path = Uri.fromFile(file);
            Intent fileIntent = new Intent(Intent.ACTION_VIEW);
            fileIntent.setDataAndType(path, "application/pdf");
            fileIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            try {
                startActivity(fileIntent);
            } catch (ActivityNotFoundException ex) {
                Toast.makeText(this, R.string.msg_pdf_opening_failed, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onMessageClicked(Message msg) {
        if (!msg.isRead()) {
            msg.setRead();
            dbManager.setMessageAsRead(msg);
        }
        replaceFragment(MessageDetailFragment.newInstance(msg), true, true);
    }

    @Override
    public void onDocumentClicked(Document doc) {
        if (!doc.isRead()) {
            doc.setRead();
            dbManager.setDocumentAsRead(doc);
        }
        if (fileExists(doc.getName())) {
            openFile(doc.getName());
        } else {
            new DownloadFile().execute(doc.getName(), doc.getUrl());
        }
    }

    @Override
    public void onEventClicked(Event evt) {
        if (!evt.isRead()) {
            evt.setRead();
            dbManager.setEventAsRead(evt);
        }
        replaceFragment(EventDetailFragment.newInstance(evt), true, true);
    }

    private class DownloadFile extends AsyncTask<String, Void, String> {

        private static final String LOG_TAG = "DownloadFile";
        private static final int MEGABYTE = 1024 * 1024;

        @Override
        protected void onPreExecute() {
            Toast.makeText(MainActivity.this, R.string.msg_downloading_file, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... strings) {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                String fileName = strings[0];
                String fileUrl = strings[1];

                File file = new File(Utilities.getInstance().getDirDocs() ,fileName);

                InputStream inputStream = null;
                FileOutputStream fileOutputStream = null;
                try {
                    if (!file.createNewFile()) {
                        Log.e(LOG_TAG, "Impossibile creare il file");
                    }

                    URL url = new URL(fileUrl);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.connect();

                    inputStream = urlConnection.getInputStream();
                    fileOutputStream = new FileOutputStream(file);

                    byte[] buffer = new byte[3 * MEGABYTE];
                    int bufferLength;
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        fileOutputStream.write(buffer, 0, bufferLength);
                    }
                    fileOutputStream.flush();
                    return fileName;
                } catch (FileNotFoundException ex) {
                    Log.e(LOG_TAG, "Impossibile trovare il file specificato", ex);
                } catch (MalformedURLException ex) {
                    Log.e(LOG_TAG, "L'url non è formattato correttamente", ex);
                } catch (IOException ex) {
                    Log.e(LOG_TAG, "Errore durante la scrittura del file", ex);
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException ignored) {
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException ignored) {
                        }
                    }
                }
            } else {
                Toast.makeText(MainActivity.this, R.string.msg_external_storage_unavailable, Toast.LENGTH_LONG).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String fileName) {
            super.onPostExecute(fileName);
            openFile(fileName);
        }

    }

}
