package it.sanmauro.budoapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class Utilities {

    private static final Utilities INSTANCE = new Utilities();
    private static final String PREFS_NAME = "userPrefs";

    public static final String KEY_EMAIL = "email";
    public static final String KEY_MSG_LAST_UPDATE = "msgLastUpdate";
    public static final String KEY_DOC_LAST_UPDATE = "docLastUpdate";
    public static final String KEY_EVT_LAST_UPDATE = "evtLastUpdate";

    public enum F { MESSAGES, DOCUMENTS, EVENTS }
    private static F currentFragment = F.MESSAGES;
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss", Locale.ITALY);

    private Utilities() {
    }

    public static Utilities getInstance() {
        return INSTANCE;
    }

    public Fragment getCurrentFragment() {
        switch (currentFragment) {
            case MESSAGES: return MessageFragment.newInstance(1);
            case DOCUMENTS: return DocumentFragment.newInstance(2);
            case EVENTS: return EventFragment.newInstance(1);
            default: return MessageFragment.newInstance(1);
        }
    }

    public void setCurrentFragment(F fragment) {
        currentFragment = fragment;
    }

    public String getStringFromSP(Context context, String key) {
        return context != null ? context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).getString(key, "") : "";
    }

    public void setStringIntoSP(Context context, String key, String value) {
        if (context != null) {
            context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit().putString(key, value).apply();
        }
    }

    public String formatDateForPrefs(Date date) {
        return this.formatter.format(date);
    }

    public String getQuery(Map<String, String> params) {
        StringBuilder query = new StringBuilder();
        boolean first = true;

        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first) {
                    first = false;
                } else {
                    query.append("&");
                }
                query.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                query.append("=");
                query.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }

        return query.toString();
    }

    private File getDirMain() {
        File dir = new File(Environment.getExternalStorageDirectory(), "Budokan");
        if (!dir.exists() && !dir.mkdir()) {
            Log.d("getDirMain", "Impossibile creare la directory");
        }
        return dir;
    }

    public File getDirDocs() {
        File dir = new File(getDirMain(), "Documenti");
        if (!dir.exists() && !dir.mkdir()) {
            Log.d("getDirDocs", "Impossibile creare la directory");
        }
        return dir;
    }

    public File getDirImgs() {
        File dir = new File(getDirMain(), "Immagini");
        if (!dir.exists() && !dir.mkdir()) {
            Log.d("getDirImgs", "Impossibile creare la directory");
        }
        return dir;
    }

    public Bitmap loadImage(String imageName) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(getDirImgs() + File.separator + imageName);
    }

}
