package it.sanmauro.budoapp.model;

import android.content.ContentValues;
import android.database.Cursor;

public class Message {

    public static final String TABLE_NAME = "messages";
    public static final String COL_ID = "id";
    public static final String COL_SUBJECT = "subject";
    public static final String COL_CONTENT = "content";
    public static final String COL_DATETIME = "datetime";
    public static final String COL_READ = "read";

    private final int id;
    private final String subject;
    private final String content;
    private final String datetime;
    private boolean read;

    public Message(int id, String subject, String content, String datetime) {
        this.id = id;
        this.subject = subject;
        this.content = content;
        this.datetime = datetime;
        this.read = false;
    }

    public Message(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(COL_ID));
        this.subject = cursor.getString(cursor.getColumnIndex(COL_SUBJECT));
        this.content = cursor.getString(cursor.getColumnIndex(COL_CONTENT));
        this.datetime = cursor.getString(cursor.getColumnIndex(COL_DATETIME));
        this.read = cursor.getInt(cursor.getColumnIndex(COL_READ)) == 1;
    }

    public int getId() {
        return this.id;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getContent() {
        return this.content;
    }

    public String getDatetime() {
        return this.datetime;
    }

    public boolean isRead() {
        return this.read;
    }

    public void setRead() {
        this.read = true;
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, this.id);
        cv.put(COL_SUBJECT, this.subject);
        cv.put(COL_CONTENT, this.content);
        cv.put(COL_DATETIME, this.datetime);
        cv.put(COL_READ, this.read);
        return cv;
    }

}
