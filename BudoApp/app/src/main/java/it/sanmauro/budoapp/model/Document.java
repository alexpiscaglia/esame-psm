package it.sanmauro.budoapp.model;

import android.content.ContentValues;
import android.database.Cursor;

public class Document {

    public static final String TABLE_NAME = "documents";
    public static final String COL_ID = "id";
    public static final String COL_URL = "url";
    public static final String COL_NAME = "name";
    public static final String COL_DATETIME = "datetime";
    public static final String COL_READ = "read";

    private final int id;
    private final String url;
    private final String name;
    private final String datetime;
    private boolean read;

    public Document(int id, String url, String name, String datetime) {
        this.id = id;
        this.url = url;
        this.name = name;
        this.datetime = datetime;
        this.read = false;
    }

    public Document(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(COL_ID));
        this.url = cursor.getString(cursor.getColumnIndex(COL_URL));
        this.name = cursor.getString(cursor.getColumnIndex(COL_NAME));
        this.datetime = cursor.getString(cursor.getColumnIndex(COL_DATETIME));
        this.read = cursor.getInt(cursor.getColumnIndex(COL_READ)) == 1;
    }

    public int getId() {
        return this.id;
    }

    public String getUrl() {
        return this.url;
    }

    public String getName() {
        return this.name;
    }

    public String getDatetime() {
        return this.datetime;
    }

    public boolean isRead() {
        return this.read;
    }

    public void setRead() {
        this.read = true;
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, this.id);
        cv.put(COL_URL, this.url);
        cv.put(COL_NAME, this.name);
        cv.put(COL_DATETIME, this.datetime);
        return cv;
    }

}
