package it.sanmauro.budoapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import it.sanmauro.budoapp.model.Event;

public class EventDetailFragment extends Fragment {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String DATETIME = "datetime";
    private static final String DATETIME_EVT = "datetimeEvt";
    private static final String ADDRESS = "address";
    private static final String DESCRIPTION = "description";
    private static final String IMAGE_NAME = "imageName";

    private String name;
    private String datetime;
    private String datetimeEvt;
    private String address;
    private String description;
    private String imageName;

    public EventDetailFragment() {
    }

    public static EventDetailFragment newInstance(Event evt) {
        EventDetailFragment fragment = new EventDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ID, evt.getId());
        args.putString(NAME, evt.getName());
        args.putString(DATETIME, evt.getDatetime());
        args.putString(DATETIME_EVT, evt.getDatetimeEvt());
        args.putString(ADDRESS, evt.getAddress());
        args.putString(DESCRIPTION, evt.getDescription());
        args.putString(IMAGE_NAME, evt.getImageName());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            datetime = getArguments().getString(DATETIME);
            datetimeEvt = getArguments().getString(DATETIME_EVT);
            address = getArguments().getString(ADDRESS);
            description = getArguments().getString(DESCRIPTION);
            imageName = getArguments().getString(IMAGE_NAME);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_detail, container, false);

        displayBackArrow(true);

        TextView lblEvtNameDetail = view.findViewById(R.id.lbl_evt_name_detail);
        TextView lblEvtInfoDetail = view.findViewById(R.id.lbl_evt_info_detail);
        ImageView imgEventDetail = view.findViewById(R.id.img_event_detail);
        TextView lblEvtDescriptionDetail = view.findViewById(R.id.lbl_evt_description_detail);

        lblEvtNameDetail.setText(name);
        lblEvtInfoDetail.setText(createInfoDetail(datetimeEvt, address));
        imgEventDetail.setImageBitmap(Utilities.getInstance().loadImage(imageName));
        lblEvtDescriptionDetail.setText(description);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        displayBackArrow(false);
    }

    private String createInfoDetail(String datetime, String address) {
        return datetime + " - " + address;
    }

    private void displayBackArrow(boolean value) {
        if ((getActivity()) != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(value);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(value);
        }
    }

}
