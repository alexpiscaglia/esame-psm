package it.sanmauro.budoapp.model;

public class Account {

    private final String name;
    private final String surname;
    private final String email;
    private final String grade;
    private final String birthDate;
    private final String certificateExpiration;

    public Account(String name, String surname, String email, String grade, String birthDate, String certificateExpiration) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.grade = grade;
        this.birthDate = birthDate;
        this.certificateExpiration = certificateExpiration;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public String getEmail() {
        return this.email;
    }

    public String getGrade() {
        return this.grade;
    }

    public String getBirthDate() {
        return this.birthDate;
    }

    public String getCertificateExpiration() {
        return this.certificateExpiration;
    }

}
