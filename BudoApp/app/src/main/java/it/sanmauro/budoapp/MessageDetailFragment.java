package it.sanmauro.budoapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.sanmauro.budoapp.model.Message;

public class MessageDetailFragment extends Fragment {

    private static final String ID = "id";
    private static final String SUBJECT = "subject";
    private static final String CONTENT = "content";
    private static final String DATETIME = "datetime";

    private String subject;
    private String content;
    private String datetime;

    public MessageDetailFragment() {
    }

    public static MessageDetailFragment newInstance(Message msg) {
        MessageDetailFragment fragment = new MessageDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ID, msg.getId());
        args.putString(SUBJECT, msg.getSubject());
        args.putString(CONTENT, msg.getContent());
        args.putString(DATETIME, msg.getDatetime());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            subject = getArguments().getString(SUBJECT);
            content = getArguments().getString(CONTENT);
            datetime = getArguments().getString(DATETIME);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_detail, container, false);

        displayBackArrow(true);

        TextView lblMsgSubjectDetail = view.findViewById(R.id.lbl_msg_subject_detail);
        TextView lblMsgContentDetail = view.findViewById(R.id.lbl_msg_content_detail);
        TextView lblMsgDatetimeDetail = view.findViewById(R.id.lbl_msg_datetime_detail);

        lblMsgContentDetail.setMovementMethod(new ScrollingMovementMethod());

        lblMsgSubjectDetail.setText(subject);
        lblMsgContentDetail.setText(content);
        lblMsgDatetimeDetail.append(datetime);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        displayBackArrow(false);
    }

    private void displayBackArrow(boolean value) {
        if (getActivity() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(value);
            //((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(value);
        }
    }

}
