package it.sanmauro.budoapp.model;

import android.content.ContentValues;
import android.database.Cursor;

public class Event {

    public static final String TABLE_NAME = "events";
    public static final String COL_ID = "id";
    public static final String COL_NAME = "name";
    public static final String COL_DATETIME = "datetime";
    public static final String COL_DATETIME_EVT = "datetime_evt";
    public static final String COL_ADDRESS = "address";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_IMAGE_NAME = "image_name";
    public static final String COL_READ = "read";

    private final int id;
    private final String name;
    private final String datetime;
    private final String datetimeEvt;
    private final String address;
    private final String description;
    private final String imageName;
    private boolean read;

    public Event(int id, String name, String datetime, String datetimeEvt, String address, String description, String imageName) {
        this.id = id;
        this.name = name;
        this.datetime = datetime;
        this.datetimeEvt = datetimeEvt;
        this.address = address;
        this.description = description;
        this.imageName = imageName;
        this.read = false;
    }

    public Event(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(COL_ID));
        this.name = cursor.getString(cursor.getColumnIndex(COL_NAME));
        this.datetime = cursor.getString(cursor.getColumnIndex(COL_DATETIME));
        this.datetimeEvt = cursor.getString(cursor.getColumnIndex(COL_DATETIME_EVT));
        this.address = cursor.getString(cursor.getColumnIndex(COL_ADDRESS));
        this.description = cursor.getString(cursor.getColumnIndex(COL_DESCRIPTION));
        this.imageName = cursor.getString(cursor.getColumnIndex(COL_IMAGE_NAME));
        this.read = cursor.getInt(cursor.getColumnIndex(COL_READ)) == 1;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDatetime() {
        return this.datetime;
    }

    public String getDatetimeEvt() {
        return this.datetimeEvt;
    }

    public String getAddress() {
        return this.address;
    }

    public String getDescription() {
        return this.description;
    }

    public String getImageName() {
        return this.imageName;
    }

    public boolean isRead() {
        return this.read;
    }

    public void setRead() {
        this.read = true;
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, this.id);
        cv.put(COL_NAME, this.name);
        cv.put(COL_DATETIME, this.datetime);
        cv.put(COL_DATETIME_EVT, this.datetimeEvt);
        cv.put(COL_ADDRESS, this.address);
        cv.put(COL_DESCRIPTION, this.description);
        cv.put(COL_IMAGE_NAME, this.imageName);
        return cv;
    }

}
