package it.sanmauro.budoapp;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.sanmauro.budoapp.DocumentFragment.OnDocumentClickedListener;
import it.sanmauro.budoapp.model.Document;

import java.util.List;

public class DocumentRecyclerViewAdapter extends RecyclerView.Adapter<DocumentRecyclerViewAdapter.ViewHolder> {

    private final List<Document> documents;
    private final OnDocumentClickedListener listener;

    public DocumentRecyclerViewAdapter(List<Document> documents, OnDocumentClickedListener listener) {
        this.documents = documents;
        this.listener = listener;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_document, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.document = documents.get(position);
        holder.lblDocName.setText(documents.get(position).getName());
        holder.lblDocDatetime.setText(documents.get(position).getDatetime());

        if (!holder.document.isRead()) {
            holder.lblDocName.setTextColor(ContextCompat.getColor(holder.view.getContext(), R.color.colorAccent));
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onDocumentClicked(holder.document);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return documents.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final View view;
        private final TextView lblDocName;
        private final TextView lblDocDatetime;
        private Document document;

        private ViewHolder(View view) {
            super(view);
            this.view = view;
            lblDocName = view.findViewById(R.id.lbl_doc_name);
            lblDocDatetime = view.findViewById(R.id.lbl_doc_datetime);
        }

    }

}
