package it.sanmauro.budoapp;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import it.sanmauro.budoapp.EventFragment.OnEventClickedListener;
import it.sanmauro.budoapp.model.Event;

import java.util.List;

public class EventRecyclerViewAdapter extends RecyclerView.Adapter<EventRecyclerViewAdapter.ViewHolder> {

    private final List<Event> events;
    private final OnEventClickedListener listener;

    public EventRecyclerViewAdapter(List<Event> events, OnEventClickedListener listener) {
        this.events = events;
        this.listener = listener;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_event, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.event = events.get(position);
        holder.lblEvtName.setText(events.get(position).getName());
        holder.lblEvtDatetime.setText(events.get(position).getDatetimeEvt());
        holder.lblEvtAddress.setText(events.get(position).getAddress());
        holder.imgEvent.setImageBitmap(Utilities.getInstance().loadImage(events.get(position).getImageName()));

        if (!holder.event.isRead()) {
            holder.lblEvtName.setTextColor(ContextCompat.getColor(holder.view.getContext(), R.color.colorAccent));
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onEventClicked(holder.event);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final View view;
        private final TextView lblEvtName;
        private final TextView lblEvtDatetime;
        private final TextView lblEvtAddress;
        private final ImageView imgEvent;
        private Event event;

        private ViewHolder(View view) {
            super(view);
            this.view = view;
            lblEvtName = view.findViewById(R.id.lbl_evt_name);
            lblEvtDatetime = view.findViewById(R.id.lbl_evt_datetime);
            lblEvtAddress = view.findViewById(R.id.lbl_evt_address);
            imgEvent = view.findViewById(R.id.img_event);
        }

    }

}
