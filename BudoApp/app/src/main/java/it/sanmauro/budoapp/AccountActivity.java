package it.sanmauro.budoapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import it.sanmauro.budoapp.model.Account;

public class AccountActivity extends AppCompatActivity {

    private TextView lblAccName;
    private TextView lblAccSurname;
    private TextView lblAccEmail;
    private TextView lblAccGrade;
    private TextView lblAccBirthDate;
    private TextView lblAccCertificateExp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this, PasswordActivity.class));
                finish();
            }
        });

        lblAccName = findViewById(R.id.lbl_acc_name);
        lblAccSurname = findViewById(R.id.lbl_acc_surname);
        lblAccEmail = findViewById(R.id.lbl_acc_email);
        lblAccGrade = findViewById(R.id.lbl_acc_grade);
        lblAccBirthDate = findViewById(R.id.lbl_acc_birth_date);
        lblAccCertificateExp = findViewById(R.id.lbl_acc_certificate_exp);

        Button btnLogout = findViewById(R.id.btn_log_out);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(AccountActivity.this)
                        .setCancelable(false)
                        .setTitle(R.string.alert_warning_title)
                        .setMessage(R.string.msg_confirm_log_out)
                        .setNegativeButton(R.string.alert_negative_btn_txt, null)
                        .setPositiveButton(R.string.alert_positive_btn_txt, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                attemptLogout();
                            }
                        }).show();
            }
        });

        new DownloadAccount().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void attemptLogout() {
        Utilities.getInstance().setStringIntoSP(AccountActivity.this, Utilities.KEY_EMAIL, "");
        Utilities.getInstance().setCurrentFragment(Utilities.F.MESSAGES);
        Intent intent = new Intent(AccountActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private class DownloadAccount extends AsyncTask<Void, Void, Account> {

        private static final String LOG_TAG = "DownloadAccount";

        private static final String JSON_SUCCESS = "success";
        private static final String JSON_DATA = "data";
        private static final String JSON_NAME = "name";
        private static final String JSON_SURNAME = "surname";
        private static final String JSON_GRADE = "grade";
        private static final String JSON_BIRTH_DATE = "birth_date";
        private static final String JSON_CERTIFICATE_EXP = "certificate_expiration";

        private static final String BASE_URL = "http://esamemobile.altervista.org/univ/account.php";

        @Override
        protected Account doInBackground(Void... params) {
            Log.d(LOG_TAG, "urlToConnect: " + BASE_URL);

            BufferedReader bufferedReader = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(BASE_URL);
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(60 * 1000);
                connection.setConnectTimeout(60 * 1000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setUseCaches(false);

                String email = Utilities.getInstance().getStringFromSP(AccountActivity.this, Utilities.KEY_EMAIL);
                Map<String, String> query = new HashMap<>();
                query.put("email", email);

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(Utilities.getInstance().getQuery(query));
                writer.flush();
                writer.close();
                os.close();

                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    StringBuilder response = new StringBuilder();
                    InputStream inputStream = connection.getInputStream();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        response.append(line);
                    }

                    JSONObject responseJson = new JSONObject(response.toString());
                    if (responseJson.getBoolean(JSON_SUCCESS)) {
                        Log.d(LOG_TAG, "success == true");

                        JSONArray array = responseJson.getJSONArray(JSON_DATA);
                        JSONObject accountObject = array.getJSONObject(0);

                        String name = accountObject.optString(JSON_NAME);
                        String surname = accountObject.optString(JSON_SURNAME);
                        String grade = accountObject.optString(JSON_GRADE);
                        String birthDate = accountObject.optString(JSON_BIRTH_DATE);
                        String certificateExp = accountObject.optString(JSON_CERTIFICATE_EXP);

                        return new Account(name, surname, email, grade, birthDate, certificateExp);
                    }
                }

            } catch (MalformedURLException ex) {
                Log.e(LOG_TAG, "L'url non è formattato correttamente", ex);
            } catch (IOException ex) {
                Log.e(LOG_TAG, "Errore durante la connessione con il server", ex);
            } catch (JSONException ex) {
                Log.e(LOG_TAG, "Errore durante la deserializzazioen della risposta", ex);
            } finally {
                if (connection != null)
                    connection.disconnect();
                try {
                    if (bufferedReader != null)
                        bufferedReader.close();
                } catch (Exception ignored) {
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Account account) {
            super.onPostExecute(account);
            if (account == null) {
                Log.d(LOG_TAG, "Errore nel download dei dati");

                new AlertDialog.Builder(AccountActivity.this)
                        .setCancelable(false)
                        .setTitle(R.string.alert_error_title)
                        .setMessage(R.string.msg_download_failed)
                        .setNeutralButton(R.string.alert_neutral_btn_txt, null)
                        .show();
            } else {
                Log.d(LOG_TAG, "Download completato correttamente");

                lblAccName.setText(account.getName());
                lblAccSurname.setText(account.getSurname());
                lblAccEmail.setText(account.getEmail());
                lblAccGrade.setText(account.getGrade());
                lblAccBirthDate.setText(account.getBirthDate());
                lblAccCertificateExp.setText(account.getCertificateExpiration());
            }
        }

    }

}
