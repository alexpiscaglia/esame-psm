package it.sanmauro.budoapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import it.sanmauro.budoapp.model.Document;
import it.sanmauro.budoapp.model.Event;
import it.sanmauro.budoapp.model.Message;

public class DatabaseManager {

    private DatabaseHelper dbHelper;

    public DatabaseManager(Context context) {
        this.dbHelper = new DatabaseHelper(context);
    }

    public boolean insertMessage(Message msg) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long row = db.insert(Message.TABLE_NAME, null, msg.getContentValues());
        db.close();
        return row > 0;
    }

    public List<Message> getMessages() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        List<Message> messages = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query = "SELECT * FROM " + Message.TABLE_NAME + " ORDER BY " + Message.COL_ID + " DESC";
            cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Message msg = new Message(cursor);
                messages.add(msg);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
        return messages;
    }

    public boolean setMessageAsRead(Message msg) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Message.COL_READ, true);
        int res = db.update(Message.TABLE_NAME, values, Message.COL_ID + " = " + msg.getId(), null);
        db.close();
        return res > 0;
    }

    public boolean insertDocument(Document doc) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long row = db.insert(Document.TABLE_NAME, null, doc.getContentValues());
        db.close();
        return row > 0;
    }

    public List<Document> getDocuments() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        List<Document> documents = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query = "SELECT * FROM " + Document.TABLE_NAME + " ORDER BY " + Document.COL_ID + " DESC";
            cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Document doc = new Document(cursor);
                documents.add(doc);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
        return documents;
    }

    public boolean setDocumentAsRead(Document doc) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Document.COL_READ, true);
        int res = db.update(Document.TABLE_NAME, values, Document.COL_ID + " = " + doc.getId(), null);
        db.close();
        return res > 0;
    }

    public boolean insertEvent(Event evt) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long row = db.insert(Event.TABLE_NAME, null, evt.getContentValues());
        db.close();
        return row > 0;
    }

    public List<Event> getEvents() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        List<Event> events = new ArrayList<>();
        Cursor cursor = null;
        try {
            String query = "SELECT * FROM " + Event.TABLE_NAME + " ORDER BY " + Event.COL_DATETIME_EVT + " ASC";
            cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Event evt = new Event(cursor);
                events.add(evt);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
        return events;
    }

    public boolean setEventAsRead(Event evt) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Event.COL_READ, true);
        int res = db.update(Event.TABLE_NAME, values, Event.COL_ID + " = " + evt.getId(), null);
        db.close();
        return res > 0;
    }

}
