package it.sanmauro.budoapp;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.sanmauro.budoapp.database.DatabaseManager;
import it.sanmauro.budoapp.model.Message;

public class MessageFragment extends Fragment {

    private static final String COLUMN_COUNT = "column-count";

    private int columnCount;
    private RecyclerView recyclerView;
    private DatabaseManager dbManager;
    private List<Message> messages;
    private OnMessageClickedListener listener;

    public MessageFragment() {
    }

    public static MessageFragment newInstance(int columnCount) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putInt(COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        columnCount = getArguments() != null ? getArguments().getInt(COLUMN_COUNT) : 1;
        dbManager = new DatabaseManager(getActivity());
        messages = new ArrayList<>();
        messages.addAll(dbManager.getMessages());

        new DownloadMessages().execute();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (columnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));
            }
            recyclerView.setAdapter(new MessageRecyclerViewAdapter(messages, listener));
            recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMessageClickedListener) {
            listener = (OnMessageClickedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnMessageClickedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public interface OnMessageClickedListener {
        void onMessageClicked(Message msg);
    }

    private class DownloadMessages extends AsyncTask<Void, Void, Boolean> {

        private static final String LOG_TAG = "DownloadMessages";

        private static final String JSON_SUCCESS = "success";
        private static final String JSON_DATA = "data";
        private static final String JSON_ID = "id";
        private static final String JSON_SUBJECT = "subject";
        private static final String JSON_CONTENT = "content";
        private static final String JSON_DATETIME = "datetime";

        private static final String BASE_URL = "http://esamemobile.altervista.org/univ/messaggi.php";

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.d(LOG_TAG, "urlToConnect: " + BASE_URL);

            BufferedReader bufferedReader = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(BASE_URL);
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(60 * 1000);
                connection.setConnectTimeout(60 * 1000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setUseCaches(false);

                Map<String, String> query = new HashMap<>();
                query.put("last_update", Utilities.getInstance().getStringFromSP(getActivity(), Utilities.KEY_MSG_LAST_UPDATE));

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(Utilities.getInstance().getQuery(query));
                writer.flush();
                writer.close();
                os.close();

                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    StringBuilder response = new StringBuilder();
                    InputStream inputStream = connection.getInputStream();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        response.append(line);
                    }

                    JSONObject responseJson = new JSONObject(response.toString());
                    if (responseJson.getBoolean(JSON_SUCCESS)) {
                        Log.d(LOG_TAG, "success == true");

                        JSONArray array = responseJson.getJSONArray(JSON_DATA);

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject messageObject = array.getJSONObject(i);

                            int id = messageObject.optInt(JSON_ID);
                            String subject = messageObject.optString(JSON_SUBJECT);
                            String content = messageObject.optString(JSON_CONTENT);
                            String datetime = messageObject.optString(JSON_DATETIME);

                            Message msg = new Message(id, subject, content, datetime);
                            dbManager.insertMessage(msg);
                        }
                        return true;
                    }
                }

            } catch (MalformedURLException ex) {
                Log.e(LOG_TAG, "L'url non è formattato correttamente", ex);
            } catch (IOException ex) {
                Log.e(LOG_TAG, "Errore durante la connessione con il server", ex);
            } catch (JSONException ex) {
                Log.e(LOG_TAG, "Errore durante la deserializzazioen della risposta", ex);
            } finally {
                if (connection != null)
                    connection.disconnect();
                try {
                    if (bufferedReader != null)
                        bufferedReader.close();
                } catch (Exception ignored) {
                }
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (success) {
                Log.d(LOG_TAG, "Download completato correttamente");

                String msgNewUpdate = Utilities.getInstance().formatDateForPrefs(Calendar.getInstance().getTime());
                Utilities.getInstance().setStringIntoSP(getActivity(), Utilities.KEY_MSG_LAST_UPDATE, msgNewUpdate);
                messages.clear();
                messages.addAll(dbManager.getMessages());
                recyclerView.getAdapter().notifyDataSetChanged();
            } else {
                Log.d(LOG_TAG, "Errore nel download dei dati");

                new AlertDialog.Builder(getActivity())
                        .setCancelable(false)
                        .setTitle(R.string.alert_error_title)
                        .setMessage(R.string.msg_download_failed)
                        .setNeutralButton(R.string.alert_neutral_btn_txt, null)
                        .show();
            }
        }

    }

}
