package it.sanmauro.budoapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import it.sanmauro.budoapp.model.Document;
import it.sanmauro.budoapp.model.Event;
import it.sanmauro.budoapp.model.Message;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "budoapp.db";
    private static final int DB_VERSION = 1;

    private static final String CREATE_TABLE_MESSAGES = "CREATE TABLE " + Message.TABLE_NAME + " (" + Message.COL_ID + " INTEGER PRIMARY KEY, " + Message.COL_SUBJECT + " TEXT, " + Message.COL_CONTENT + " TEXT, " + Message.COL_DATETIME + " TEXT, " + Message.COL_READ + " INTEGER)";
    private static final String CREATE_TABLE_DOCUMENTS = "CREATE TABLE " + Document.TABLE_NAME + " (" + Document.COL_ID + " INTEGER PRIMARY KEY, " + Document.COL_URL +  " TEXT, " + Document.COL_NAME + " TEXT, " + Document.COL_DATETIME + " TEXT, " + Document.COL_READ + " INTEGER)";
    private static final String CREATE_TABLE_EVENTS = "CREATE TABLE " + Event.TABLE_NAME + " (" + Event.COL_ID + " INTEGER PRIMARY KEY, " + Event.COL_NAME + " TEXT, " + Event.COL_DATETIME + " TEXT, " + Event.COL_DATETIME_EVT + " TEXT, " + Event.COL_ADDRESS + " TEXT, " + Event.COL_DESCRIPTION + " TEXT, " + Event.COL_IMAGE_NAME + " TEXT, " + Event.COL_READ + " INTEGER)";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MESSAGES);
        db.execSQL(CREATE_TABLE_DOCUMENTS);
        db.execSQL(CREATE_TABLE_EVENTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
