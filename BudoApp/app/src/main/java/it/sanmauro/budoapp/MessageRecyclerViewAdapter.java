package it.sanmauro.budoapp;

import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.sanmauro.budoapp.MessageFragment.OnMessageClickedListener;
import it.sanmauro.budoapp.model.Message;

import java.util.List;

public class MessageRecyclerViewAdapter extends RecyclerView.Adapter<MessageRecyclerViewAdapter.ViewHolder> {

    private final List<Message> messages;
    private final OnMessageClickedListener listener;

    public MessageRecyclerViewAdapter(List<Message> messages, MessageFragment.OnMessageClickedListener listener) {
        this.messages = messages;
        this.listener = listener;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.message = messages.get(position);
        holder.lblMsgSubject.setText(messages.get(position).getSubject());
        holder.lblMsgDatetime.setText(messages.get(position).getDatetime());
        holder.lblMsgContent.setText(messages.get(position).getContent());

        if (!holder.message.isRead()) {
            holder.lblMsgSubject.setTextColor(ContextCompat.getColor(holder.view.getContext(), R.color.colorAccent));
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    listener.onMessageClicked(holder.message);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final View view;
        private final TextView lblMsgSubject;
        private final TextView lblMsgDatetime;
        private final TextView lblMsgContent;
        private Message message;

        private ViewHolder(View view) {
            super(view);
            this.view = view;
            this.lblMsgSubject = view.findViewById(R.id.lbl_msg_subject);
            this.lblMsgDatetime = view.findViewById(R.id.lbl_msg_datetime);
            this.lblMsgContent = view.findViewById(R.id.lbl_msg_content);
        }

    }

}
