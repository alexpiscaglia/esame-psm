package it.sanmauro.budoapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class PasswordActivity extends AppCompatActivity {

    private EditText edtOldPwd;
    private EditText edtNewPwd;
    private EditText edtConfirmPwd;
    private View pwdProgress;
    private View pwdForm;

    private UpdatePasswordTask updatePwdTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        edtOldPwd = findViewById(R.id.edt_old_pwd);
        edtNewPwd = findViewById(R.id.edt_new_pwd);
        edtConfirmPwd = findViewById(R.id.edt_confirm_pwd);
        pwdProgress = findViewById(R.id.pwd_progress);
        pwdForm = findViewById(R.id.pwd_form);

        Button btnChangePwd = findViewById(R.id.btn_change_pwd);
        btnChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptChangePwd();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void attemptChangePwd() {
        if (updatePwdTask != null) {
            return;
        }

        edtOldPwd.setError(null);
        edtNewPwd.setError(null);
        edtConfirmPwd.setError(null);

        String oldPwd = edtOldPwd.getText().toString();
        String newPwd = edtNewPwd.getText().toString();
        String confirmPwd = edtConfirmPwd.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(oldPwd)) {
            edtOldPwd.setError(getString(R.string.error_field_required));
            focusView = edtOldPwd;
            cancel = true;
        } else if (!isPasswordValid(oldPwd)) {
            edtOldPwd.setError(getString(R.string.error_invalid_password));
            focusView = edtOldPwd;
            cancel = true;
        } else if (TextUtils.isEmpty(newPwd)) {
            edtNewPwd.setError(getString(R.string.error_field_required));
            focusView = edtNewPwd;
            cancel = true;
        } else if (!isPasswordValid(newPwd)) {
            edtNewPwd.setError(getString(R.string.error_invalid_password));
            focusView = edtNewPwd;
            cancel = true;
        } else if (TextUtils.isEmpty(confirmPwd)) {
            edtConfirmPwd.setError(getString(R.string.error_field_required));
            focusView = edtConfirmPwd;
            cancel = true;
        } else if (!isPasswordValid(confirmPwd)) {
            edtConfirmPwd.setError(getString(R.string.error_invalid_password));
            focusView = edtConfirmPwd;
            cancel = true;
        } else if (!newPwd.equals(confirmPwd)) {
            edtConfirmPwd.setError(getString(R.string.error_pwd_not_matching));
            focusView = edtConfirmPwd;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            updatePwdTask = new UpdatePasswordTask(oldPwd, newPwd);
            updatePwdTask.execute();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        pwdForm.setVisibility(show ? View.GONE : View.VISIBLE);
        pwdForm.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                pwdForm.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        pwdProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        pwdProgress.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                pwdProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    private class UpdatePasswordTask extends AsyncTask<Void, Void, Boolean> {

        private static final String LOG_TAG = "UpdatePasswordTask";
        private static final String JSON_SUCCESS = "success";
        private static final String BASE_URL = "http://esamemobile.altervista.org/univ/update_pwd.php";

        private final String oldPassword;
        private final String newPassword;

        UpdatePasswordTask(String oldPassword, String newPassword) {
            this.oldPassword = oldPassword;
            this.newPassword = newPassword;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.d(LOG_TAG, "urlToConnect: " + BASE_URL);

            BufferedReader bufferedReader = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(BASE_URL);
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(60 * 1000);
                connection.setConnectTimeout(60 * 1000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setUseCaches(false);

                Map<String, String> query = new HashMap<>();
                query.put("email", Utilities.getInstance().getStringFromSP(PasswordActivity.this, Utilities.KEY_EMAIL));
                query.put("oldPwd", oldPassword);
                query.put("newPwd", newPassword);

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(Utilities.getInstance().getQuery(query));
                writer.flush();
                writer.close();
                os.close();

                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    StringBuilder response = new StringBuilder();
                    InputStream inputStream = connection.getInputStream();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        response.append(line);
                    }

                    JSONObject responseJson = new JSONObject(response.toString());
                    return responseJson.getBoolean(JSON_SUCCESS);
                }

            } catch (MalformedURLException ex) {
                Log.e(LOG_TAG, "L'url non è formattato correttamente", ex);
            } catch (IOException ex) {
                Log.e(LOG_TAG, "Errore durante la connessione con il server", ex);
            } catch (JSONException ex) {
                Log.e(LOG_TAG, "Errore durante la deserializzazioen della risposta", ex);
            } finally {
                if (connection != null)
                    connection.disconnect();
                try {
                    if (bufferedReader != null)
                        bufferedReader.close();
                } catch (Exception ignored) {
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            updatePwdTask = null;
            showProgress(false);

            if (success == null) {
                new AlertDialog.Builder(PasswordActivity.this)
                        .setCancelable(false)
                        .setTitle(R.string.alert_error_title)
                        .setMessage(R.string.msg_download_failed)
                        .setNeutralButton(R.string.alert_neutral_btn_txt, null)
                        .show();
            } else if (success) {
                new AlertDialog.Builder(PasswordActivity.this)
                        .setCancelable(false)
                        .setTitle(R.string.alert_success_title)
                        .setMessage(R.string.msg_pwd_change_success)
                        .setNeutralButton(R.string.alert_neutral_btn_txt, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(PasswordActivity.this, AccountActivity.class));
                                finish();
                            }
                        })
                        .show();
            } else {
                new AlertDialog.Builder(PasswordActivity.this)
                        .setCancelable(false)
                        .setTitle(R.string.alert_error_title)
                        .setMessage(R.string.msg_pwd_change_failed)
                        .setNeutralButton(R.string.alert_neutral_btn_txt, null)
                        .show();
            }
        }

        @Override
        protected void onCancelled() {
            updatePwdTask = null;
            showProgress(false);
        }

    }

}
